$(document).ready(function(){
    $('.mobile-icon').click(function(){
        $('.menu').slideToggle(500);
    });
});

/*slider*/
var current_slide = 1;
setInterval(function(){
    document.getElementById('slides_' + current_slide).style.display = 'block';
    document.getElementById('slides_' + current_slide).style.transition = 'all 0.5s';
    document.getElementById('slides_' + current_slide).style.display = 'none';
    current_slide += 1;
    if(current_slide === 5){
        current_slide = 1;
    }
    document.getElementById('slides_' + current_slide).style.display = 'block';
}, 5000);

/*menu*/