var click = 0;
if($(window).width() > 1024){
    $('#shop').hover(
        function(){
            $('#submenu-shop').css('display', 'table-cell');
        },
        function(){
            $('#submenu-shop').css('display', 'none');
        },
    );
    $('#submenu-shop').hover(
        function(){
            $('#submenu-shop').css('display', 'table-cell');
        },
        function(){
            $('#submenu-shop').css('display', 'none');
        },
    );

    $('#collections').hover(
        function(){
            $('#submenu-collection').css('display', 'table-cell');
        },
        function(){
            $('#submenu-collection').css('display', 'none');
        },
    );
    $('#submenu-collection').hover(
        function(){
            $('#submenu-collection').css('display', 'table-cell');
        },
        function(){
            $('#submenu-collection').css('display', 'none');
        },
    );
}
else{
    $('#shop').click(function(){
        if(click == 0){
            $('#submenu-shop').css('display', 'block');
            click += 1;
        }
        else{
            $('#submenu-shop').css('display', 'none');
            click -= 1;
        }
    });
    $('#collections').click(function(){
        if(click == 0){
            $('#submenu-collection').css('display', 'block');
            click += 1;
        }
        else{
            $('#submenu-collection').css('display', 'none');
            click -= 1;
        }
    });
}
    